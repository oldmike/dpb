## Contents

* [DPB-Book](#dpb-book)
* [Scripts](#scripts)
* [Dependencies](#dependencies)
* [Translators](#translators)
* [License](#license)

# DPB-Book

The book is recently available in German. It is written in NoWeb and
contains the script.

The script create-book.sh generates the book in the formats pdf and epub.

The generated book can be downloaded as [pdf][] and [epub][].

# Scripts

All scripts are available with a user interface in English. The main script
can help to build Debian packages using git-buildpackage.

With the script create-buildscript.sh the main script [build-gbp.sh][] and
other scripts like plugins can be generated. The following plugins are
available:

* [build-gbp-java-plugin.sh][]
* [build-gbp-maven-plugin.sh][]
* [build-gbp-webext-plugin.sh][]
* [build-gbp-python-plugin.sh][]

# Dependencies

To extract the program script and the book in pdf and epub format you
have to install the following packages:

For the script:
* noweb

and additional for creating the book

* texlive
* texlive-bibtex-extra
* texlive-binaries
* texlive-extra-utils
* texlive-lang-german
* texlive-lang-japanese
* texlive-latex-extra
* biber
* tidy

To use the scripts you have to install the dependencies listed in the
headers of the scripts.

# Translators

The project switched from po4a to OmegaT because it is well documented 
and more convenient.

To get started:

First you need to install additionally:

    sudo apt install omegat texlive-lang-<your language>

Then you have to create a working directory:

    git clone https://salsa.debian.org/ddp-team/dpb
    cd dpb

Localized files have to be filed under 

    translation/<ISO-Code>. 

Make that directory if not exists.

Documentation of OmegaT is available in many languages under
/usr/share/doc/omegat/html/.

# License

The book has the following license:

Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen
4.0 International Lizenz (CC BY-SA 4.0)
https://creativecommons.org/licenses/by-sa/4.0/legalcode

The code is licensed under GNU General Public License Version 3 or
(at your option) any later version

[pdf]: https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf
[epub]: https://ddp-team.pages.debian.net/dpb/BuildWithGBP.epub
[build-gbp.sh]: https://ddp-team.pages.debian.net/dpb/build-gbp.sh
[build-gbp-java-plugin.sh]:  https://ddp-team.pages.debian.net/dpb/build-gbp-java-plugin.sh
[build-gbp-maven-plugin.sh]:  https://ddp-team.pages.debian.net/dpb/build-gbp-maven-plugin.sh
[build-gbp-webext-plugin.sh]:  https://ddp-team.pages.debian.net/dpb/build-gbp-webext-plugin.sh
[build-gbp-python-plugin.sh]:   https://ddp-team.pages.debian.net/dpb/build-gbp-python-plugin.sh
