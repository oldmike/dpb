#!/bin/bash

set -e
set -o pipefail

which file >/dev/null

if file *.nw | grep -qs "with CRLF"; then
   echo "Error: Windows line endings detected:"
   file *.nw | grep "with CRLF"
   exit 1
fi

exit 0
